import { Injectable, NotFoundException } from '@nestjs/common';
import { Product } from './product.model';


@Injectable()
export class ProductsService {
    private products: Product[] = [];
    private counter = 1;
    insertProduct(title: string, desc: string, price: number){
        const prodId = this.counter.toString();
        this.counter = this.counter+1;
        const newProduct = new Product(prodId, title, desc, price);
        this.products.push(newProduct);
        return prodId;
    }

    getProducts() {
        return [...this.products];
    }

    getSingleProduct(productId: string) {
        const product = this.findProduct(productId)[0];
        return { ...product };
    }

    updateProduct(
        productId: string, 
        productTitle: string, 
        productDesc: string,
        productPrice: number) {

            const [product, index] = this.findProduct(productId);
            const updatedProduct =  {...product};
            if(productTitle){
                updatedProduct.title = productTitle;
            }
            if(productDesc){
                updatedProduct.description = productDesc;
            }
            if(productPrice){
                updatedProduct.price = productPrice;
            }
            this.products[index] = updatedProduct;
    }

    private findProduct(productId: string): [Product, number] {
        const productIndex = this.products.findIndex(prod => prod.id == productId);
        const product = this.products[productIndex];
            if(!product){
                throw new NotFoundException('Could not find product');
            }
            return [product, productIndex];
    }

    deleteProduct(productId: string) {
        const index = this.findProduct(productId)[1];
        this.products.splice(index,1);
    }

}
